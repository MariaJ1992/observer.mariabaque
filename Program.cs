﻿using System;

namespace Observer.MariaBaque
{
    class Program
    {
        static void Main(string[] args)
        {
            //Se procede a instanciar los objetos.
            PlatformNeptuno novedades = new PlatformNeptuno();

            //Se agregan observadores haciendo uso del metodo Añadir y se establecen los datos correspondientes.
            Empleado empleado1 = new Empleado();
            empleado1.BodegaDepart = "Bodega";
            empleado1.NombreDelEmpleado = "Velasquez Marcillo Jose";
            novedades.Añadir(empleado1);

            Empleado empleado2 = new Empleado();
            empleado2.BodegaDepart = "Bodega";
            empleado2.NombreDelEmpleado = "Cedeño Franco Laura";
            novedades.Añadir(empleado2);

            //Se establecen los cambios de estado en el sujeto, en este caso las actividades.
            novedades.Actividad = "InventariosDeProductos";
            novedades.Actividad = "MercaderiaNueva";
        }
    }
}
