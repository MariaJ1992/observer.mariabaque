﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer.MariaBaque
{
        //Se establece la herencia con la interfaz observador.
        class Empleado : IObservador
    {
        //Se declaran las siguientes variables.
        public string BodegaDepart { get; set; }
        public string NombreDelEmpleado { get; set; }

        //Se hace uso del metodo Actualizar de la interfaz observador.
        public void Actualizar(ISujeto sujeto)
        {
            if (sujeto is PlatformNeptuno novedades)
            {
                //Se establecen los mensajes de notificación que recibiran los observadores con cada actualización que se de.
                Console.WriteLine("\nEstimado " + NombreDelEmpleado + ", en el departamento de " + BodegaDepart + " existe nueva convocatoria a reunión.");
                Console.WriteLine("Se publicó convocatoria de: " + novedades.Actividad);
            }
        }
    }
}
