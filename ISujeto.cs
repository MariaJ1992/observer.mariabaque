﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer.MariaBaque
{
       //Se establece la interfaz sujeto y se establecen los metodos.
       interface ISujeto
    {
        //Metodo Añadir, permitirá agregar un observador.
        public void Añadir(IObservador observador);

        //Metodo Eliminar, permitirá eliminar un observador.
        public void Eliminar(IObservador observador);
            
        //Metodo Notificar, avisa los cambios que se hacen en el sujeto.
        public void Notificar();
    }
}
