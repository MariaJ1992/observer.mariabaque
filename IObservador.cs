﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer.MariaBaque
{   
            //Se establece la interfaz observador y se establece el metodo.
            interface IObservador
    {
            //Metodo actualizar realizará la actualización según los cambios del sujeto.
            public void Actualizar(ISujeto sujeto);
        }
    }